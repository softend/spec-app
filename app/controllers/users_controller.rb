class UsersController < ApplicationController
  def new #method register user new
    @user = User.new
  end
  
  def create #moethod post for new user ตรวจความถูกตอนกรอกฟอร์ม
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "Welcome to the Spec-pc"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def show
    @user = User.find(params[:id])
    
  end
  
  
  private
  def user_params
    params.require(:user).permit(:name,:email,
    :password,:password_confirmation)
  end
end
